import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

//import "./App.css";
import LoginPage from "./pages/LoginPage";
import "antd/dist/antd.css";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={LoginPage} />
      </Switch>
    </Router>
  );
}

export default App;
